#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "leconnlog.hpp"
#include "CLI11.hpp"

extern "C" {
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
};
#include <arpa/inet.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>

using namespace std;

#define FLOW_REPORT_TIME 30
#define FLOW_QUEUE_MAX 200000
#define INVALIDATE_QUEUE_MAX 200000
#define BULK_RECORDS 1024

class Global {
public:
	std::string searchip;
	std::string csv_filename;
	ofstream csv;
	bool csv_output;
	bool test;
	bool localtime;
	enum class Mode {
		NONE,
		SHOWALL,
		SEARCHIP
	};
	Mode mode;
	Global();
};

Global::Global() {
	mode = Mode::NONE;
	test = false;
};

void die_handler(int s) {
	exit(0);
}

void process_file(Global *g, std::string filename) {
	struct f_header hdr;
	struct tm * timeinfo;
	time_t ts;

	struct f_record rec;
	uint32_t seq = 0;
	uint32_t offset_next;
	int offset_sec = 0;
	char datebuf[64];
	bool tempfile = false;

	int ret;

	// does file have xz suffix?
	if (filename.substr(filename.length() - 3) == ".xz") {
		// generate temporary file
		// we can do native with liblzma, but this is easier, for now
		char tmpfilename[256];
		printf("Decompressing %s\n", filename.c_str());
		sprintf(tmpfilename, "/tmp/leconnview-%d", getpid());
		string cmd = "xz -d -c " + filename + " > " + tmpfilename;
		system(cmd.c_str());
		filename = tmpfilename;
		tempfile = true;
	}

	int fd = open(filename.c_str(), O_RDONLY);
	if (fd < 0) {
		cout << "Error opening records" << endl;
		exit(1);
	}
	ret = read(fd, &hdr, sizeof(hdr));
	if (ret == sizeof(hdr)) {
		offset_next = hdr.offset_map[0]; // always 0
		ts = (time_t) hdr.init_ts;
		if (g->localtime) {
			timeinfo = localtime (&ts);
		} else {
			timeinfo = gmtime (&ts);
		}

		printf ("Begins %s", asctime(timeinfo));
	}

	while (read(fd, &rec, sizeof(rec)) == sizeof(rec)) {
		if (seq == offset_next) {
			ts++;
			if (g->localtime) {
				timeinfo = localtime (&ts);
			} else {
				timeinfo = gmtime (&ts);
			}
			strftime(datebuf, 64, "%F %T", timeinfo);
			offset_sec++;
			offset_next = hdr.offset_map[offset_sec + 1];
		} else {
			int len;
			sprintf(datebuf, "~%s", asctime(timeinfo));
			len = strlen(datebuf);
			datebuf[len - 1] = 0x0; // remove newline
		}

		if (!g->searchip.empty()) {
			if (IsIPInRangeExt(iptostring(rec.ipsrc), g->searchip) == false && IsIPInRangeExt(iptostring(rec.ipdst), g->searchip) == false) {
				seq++;
				continue;
			}
		}
		// if csv_filename defined, output to csv
		if (!g->csv_filename.empty()) {
			g->csv << datebuf << "," << int(rec.type) << "," << iptostring(rec.ipsrc) << "," << rec.portsrc << "," << iptostring(rec.ipnatsrc) << "," << rec.natportsrc << "," << iptostring(rec.ipdst) << "," << rec.portdst << endl;
		} else {
			if (rec.ipnatsrc == 0) {
				cout << "[" << datebuf << "] " << int(rec.type) << " " << iptostring(rec.ipsrc) << ":" << rec.portsrc << "(N/A)" <<
			    	 " > " << iptostring(rec.ipdst) << ":" << rec.portdst << endl;
			} else {
				cout << "[" << datebuf << "] " << int(rec.type) << " " << iptostring(rec.ipsrc) << ":" << rec.portsrc << "(" << iptostring(rec.ipnatsrc) << ":" << rec.natportsrc << ")" <<
			    	 " > " << iptostring(rec.ipdst) << ":" << rec.portdst << endl;
			}
		}
		seq++;
	}


	if (tempfile) {
		unlink(filename.c_str());
	}

	close(fd);
}

int main(int argc, char *argv[]) {
	Global g;
	CLI::App app{"LE log viewer v1.1"};
	std::string filename;
	std::string directory;

	app.add_option("-d,--directory", directory, "View or convert all files in directory");
	app.add_option("-f,--file", filename, "Filename to view")->check(CLI::ExistingFile);
	app.add_option("-o,--output", g.csv_filename, "Output csv filename");
	app.add_flag("--localtime", g.localtime, "Convert GMT to local time");
	app.add_option("--ip", g.searchip, "IP to search");
	app.add_flag("--test", g.test, "Test mode");

	CLI11_PARSE(app, argc, argv);

	if (g.test) {
		cout << "Test mode" << endl;
		return 0;
	}

	// if csv defined, open g.csv_filename
	if (!g.csv_filename.empty()) {
		g.csv_output = true;
		g.csv.open(g.csv_filename);
	}
	// if directory, read all files in directory
	if (!directory.empty()) {
		DIR *dir;
		struct dirent *ent;
		if ((dir = opendir(directory.c_str())) != NULL) {
			while ((ent = readdir(dir)) != NULL) {
				if (ent->d_type == DT_REG) {
					filename = directory + "/" + ent->d_name;
					process_file(&g, filename);
				}
			}
			closedir(dir);
		} else {
			cout << "Error opening directory" << endl;
			exit(1);
		}
		return 0;
	} else {
		process_file(&g, filename);
	}
	// if csv defined, close g.csv_filename
	if (!g.csv_filename.empty()) {
		g.csv.close();
	}

	return 0;
}