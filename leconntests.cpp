#include <string>
#include <iostream>
#include <tgmath.h>
#include <arpa/inet.h>
#include "leconnlog.hpp"

int main(int argc, char *argv[]) {
	uint32_t testnum = 0;
	if (IsIPInRange("1.2.3.4", "1.2.3.0", "24") != true) {
		std::cout << "Error in test " << testnum << std::endl;
		return 1;
	}
	testnum++;

	if (IsIPInRange("1.2.3.4", "1.2.3.0", "255.255.255.0") != true) {
		std::cout << "Error in test " << testnum << std::endl;
		return 1;
	}
	testnum++;

	if (IsIPInRange("1.2.4.4", "1.2.3.0", "24") != false) {
		std::cout << "Error in test " << testnum << std::endl;
		return 1;
	}
	testnum++;

	if (IsIPInRange("1.2.4.4", "1.2.3.0", "255.255.255.0") != false) {
		std::cout << "Error in test " << testnum << std::endl;
		return 1;
	}
	testnum++;

	uint32_t maskip = (0xFFFFFFFF << (32 - 24)) & 0xFFFFFFFF;;
	if (iptostring(maskip) != "255.255.255.0") {
		std::cout << "Error in test " << testnum << std::endl;
		return 1;
	}
	testnum++;

	maskip = (0xFFFFFFFF << (32 - 22)) & 0xFFFFFFFF;
	//std::cout << iptostring(maskip) << std::endl;
	if (iptostring(maskip) != "255.255.252.0") {
		std::cout << "Error in test " << testnum << std::endl;
		return 1;
	}
	testnum++;




	return 0;
}