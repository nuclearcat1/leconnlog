#include <cstdint>
#include <string>
#include <vector>
#include <tgmath.h>
#include <assert.h>
#include <arpa/inet.h> // TODO: replace by bitswap operations

std::vector<std::string> split(const std::string& s, char delimiter)
{
	std::vector<std::string> tokens;
	std::string token;
	std::istringstream tokenStream(s);
	while (std::getline(tokenStream, token, delimiter))
	{
		tokens.push_back(token);
	}
	return tokens;
}

uint32_t ip2uint(const std::string ip) {
	int a, b, c, d;
	uint32_t addr = 0;

	if (sscanf(ip.c_str(), "%d.%d.%d.%d", &a, &b, &c, &d) != 4)
		return 0;

	addr = a << 24;
	addr |= b << 16;
	addr |= c << 8;
	addr |= d;
	return addr;
}

// TODO: Add verification if network/mask is correct
bool IsIPInRange(const std::string ip, const std::string network, const std::string mask) {
	uint32_t ip_addr = ip2uint(ip);
	uint32_t network_addr = ip2uint(network);
	uint32_t mask_addr;
	// IP form or netnum?
	if (mask.find('.') != std::string::npos) {
		mask_addr = ip2uint(mask);
	} else {
		uint32_t mask_int = std::stoi(mask);
		mask_addr = (0xFFFFFFFF << (32 - mask_int)) & 0xFFFFFFFF;
		//mask_addr = pow(2, mask_int);
	}

	uint32_t net_lower = (network_addr & mask_addr);
	uint32_t net_upper = (net_lower | (~mask_addr));

	if (ip_addr >= net_lower &&
	        ip_addr <= net_upper)
		return true;
	return false;
}

bool IsIPInRangeExt(const std::string ip, std::string ipnetmask) {
	if (ipnetmask.find('/') == std::string::npos) {
		std::string msk32 = "/32";
		ipnetmask.append(msk32);
	}
	std::vector<std::string> tokens = split(ipnetmask, '/');
	assert(tokens.size() == 2);
	return IsIPInRange(ip, tokens[0], tokens[1]);
}

std::string iptostring (uint32_t ipAddress) {
	char ipAddr[16];
	snprintf(ipAddr, sizeof ipAddr, "%u.%u.%u.%u" , (ipAddress & 0xff000000) >> 24
	         , (ipAddress & 0x00ff0000) >> 16
	         , (ipAddress & 0x0000ff00) >> 8
	         , (ipAddress & 0x000000ff));
	return ipAddr;
}
