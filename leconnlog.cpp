#define HYDROGEN_CONTEXT "CONNLOGG"
#include "hydrogen.h"

/*
	TODO: optimize match/ignore, less string conversion, do it at start
	TODO: handle kill properly and write indexes in open file
*/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "leconnlog.hpp"
#include "concurrentqueue.h"

extern "C" {
#include <glib.h>
#include <fixbuf/public.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
};
#include <arpa/inet.h>
#include <time.h>
#include "CLI11.hpp"

using namespace std;

#define FLOW_REPORT_TIME 30
#define FLOW_QUEUE_MAX 200000
#define INVALIDATE_QUEUE_MAX 200000
#define BULK_RECORDS 1024

/*
flowEndReason
0x01 	Idle Timeout 	The Flow was terminated because it was considered to be idle.
0x02 	Active Timeout 	The Flow was terminated for reporting purposes while it was still active, for example, after the maximum lifetime of unreported Flows was reached.
0x03 	End of Flow Detected 	The Flow was terminated because the Metering Process detected signals indicating the end of the Flow, for example, the TCP FIN flag.
0x04 	Forced End 	The Flow was terminated because of some external event, for example, a shutdown of the Metering Process initiated by a network management application.
0x05 	Lack of Resources 	The Flow was terminated because of lack of resources available to the Metering Process and/or the Exporting Process.
*/

extern "C" {
	static fbInfoElementSpec_t simpleTemplate[] = {
		{"systemInitTimeMilliseconds",          8, 0 },
		{"observationTimeMilliseconds",         8, 0 },
		{"flowStartMilliseconds",               8, 0 },
		{"flowEndMilliseconds",                 8, 0 },
		{"octetTotalCount",                     8, 0 },
		{"octetDeltaCount",                     8, 0 },
		{"reverseOctetTotalCount",              8, 0 },
		{"packetTotalCount",                    8, 0 },
		{"reversePacketTotalCount",             8, 0 },
		{"sourceIPv4Address",                   4, 0 },
		{"destinationIPv4Address",              4, 0 },
		{"postNATSourceIPv4Address",            4, 0 },
		{"postNATDestinationIPv4Address",       4, 0 },
		{"sourceTransportPort",                 2, 0 },
		{"destinationTransportPort",            2, 0 },
		{"postNAPTSourceTransportPort",         2, 0 },
		{"postNAPTDestinationTransportPort",    2, 0 },
		{"protocolIdentifier",                  1, 0 },
		{"flowEndReason",                       1, 0 },
		{"natEvent",                            1, 0 },
		FB_IESPEC_NULL
	};
}

typedef struct simpleRecord_st {
	uint64_t    systemInitTimeMilliseconds;
	uint64_t    observationTimeMilliseconds;
	uint64_t    flowStartMilliseconds;
	uint64_t    flowEndMilliseconds;
	uint64_t    octetTotalCount;
	uint64_t    octetDeltaCount;
	uint64_t    reverseOctetTotalCount;
	uint64_t    packetTotalCount;
	uint64_t    reversePacketTotalCount;
	uint32_t    sourceIPv4Address;
	uint32_t    destinationIPv4Address;
	uint32_t    postNATSourceIPv4Address;
	uint32_t    postNATDestinationIPv4Address;
	uint16_t    sourceTransportPort;
	uint16_t    destinationTransportPort;
	uint16_t	postNAPTSourceTransportPort;
	uint16_t	postNAPTDestinationTransportPort;
	uint8_t     protocolIdentifier;
	uint8_t     flowEndReason;
	uint8_t     natEvent;
} simpleRecord_t;

void die_handler(int s) {
	exit(0);
}

void error_handler (GError *err, int line) {
	int critical = 0;
	if (err == NULL)
		return;

	fprintf (stderr, "ERROR[%d]: %s\n", line, err->message);

	if (!critical) {
		g_error_free(err);
		return;
	}

	exit(0);
}

class MainClass {
public:
	MainClass(void);
	~MainClass();
	void NetFlowThread(void);
	void FlowProcessor(void);
	bool is_netflow;
	int runthreads;
	int f_mode;
	bool debug;
	bool test;
	std::string bindport;
	std::string bindip;
	std::string dir;
	std::vector<std::string> ignoresrc;
	std::vector<std::string> ignoredst;
	std::vector<std::string> ignoreip;
	std::vector<int> ignoreproto;
	std::vector<int> ignoreport;
	std::vector<int> ignoresrcport;
	std::vector<int> ignoredstport;
	std::vector<std::string> matchsrc;
	std::vector<std::string> matchdst;
	std::vector<std::string> matchip;
	std::vector<int> matchproto;
	std::vector<int> matchport;
	std::vector<int> matchsrcport;
	std::vector<int> matchdstport;
	moodycamel::ConcurrentQueue<simpleRecord_t*> *flow_queue;
	/* Metrics */
	uint16_t metrics_port = 0;
	std::string instance_name = "leconnlog";
	uint64_t flow_count;
};

MainClass::MainClass(void) {
	is_netflow = false;
	runthreads = 1;
	bindport = "2055";
	bindip = "0.0.0.0";
	dir = "./";
	flow_queue = new moodycamel::ConcurrentQueue<simpleRecord_t*>(FLOW_QUEUE_MAX * 2);
	debug = false;

	// if environment have set IGNOREDSTNET="10.0.0.0/8,192.168.0.0/16,100.16.0.0/12"
	// add to ignoredst vector
	char *ignoredstnetenv = getenv("IGNOREDSTNET");
	if (ignoredstnetenv != NULL) {
		std::string ignoredstnetstr(ignoredstnetenv);
		std::stringstream ss(ignoredstnetstr);
		std::string item;
		while (std::getline(ss, item, ',')) {
			cout << "Adding ignoredstnet: " << item << endl;
			ignoredst.push_back(item);
		}
	}
	// IGNOREDSTPORT same as IGNOREDSTNET but ignoredstport
	char *ignoredstportenv = getenv("IGNOREDSTPORT");
	if (ignoredstportenv != NULL) {
		std::string ignoredstportstr(ignoredstportenv);
		std::stringstream ss(ignoredstportstr);
		std::string item;
		while (std::getline(ss, item, ',')) {
			cout << "Adding ignoredstport: " << item << endl;
			int port = atoi(item.c_str());
			ignoredstport.push_back(port);
		}
	}
	// LOGSRC to matchsrc
	char *logsrcenv = getenv("LOGSRC");
	if (logsrcenv != NULL) {
		std::string logsrcstr(logsrcenv);
		std::stringstream ss(logsrcstr);
		std::string item;
		while (std::getline(ss, item, ',')) {
			cout << "Adding logsrc: " << item << endl;
			matchsrc.push_back(item);
		}
	}
};

MainClass::~MainClass() {
	delete(flow_queue);
}

void MainClass::NetFlowThread(void) {
	simpleRecord_t *recpool[FLOW_QUEUE_MAX * 2];
	simpleRecord_t *nextrecord = NULL;
	GError *err = NULL;
	int rc = 0;

	int poolpos = 0;

	for (int i = 0; i < FLOW_QUEUE_MAX * 2; i++) {
		recpool[i] = (simpleRecord_t*)malloc(sizeof(simpleRecord_t));
	}
	err = NULL;

	fbInfoModel_t *infoModel = fbInfoModelAlloc();

	fbSession_t *session = fbSessionAlloc(infoModel);
	fbTemplate_t *tmpl = fbTemplateAlloc(infoModel);

	fbTemplateAppendSpecArray(tmpl, simpleTemplate, 0xffffffff, &err);
	error_handler(err, __LINE__);

	uint16_t collectorTemplateID = fbSessionAddTemplate(session, TRUE, FB_TID_AUTO, tmpl, &err);
	error_handler(err, __LINE__);

	struct fbConnSpec_st socketDef;
	socketDef.transport = FB_UDP;
	socketDef.host = strdup(bindip.c_str());
	socketDef.svc = strdup(bindport.c_str());
	socketDef.ssl_ca_file = NULL;
	socketDef.ssl_cert_file = NULL;
	socketDef.ssl_key_file = NULL;
	socketDef.ssl_key_pass = NULL;
	socketDef.vai = NULL;
	socketDef.vssl_ctx = NULL;

	fbListener_t *collectorListener = fbListenerAlloc(&socketDef, session, NULL, NULL, &err);
	error_handler(err, __LINE__);
	if (is_netflow) {
		fbCollector_t *col;
		if (fbListenerGetCollector(collectorListener, &col, &err) == TRUE) {
			error_handler(err, __LINE__);
			fbCollectorSetNetflowV9Translator(col, &err);
			error_handler(err, __LINE__);
		} else {
			error_handler(err, __LINE__);
			printf("Collector not found?\n");
			exit(0);
		}
	}

	fBuf_t *collectorBuf = fbListenerWait(collectorListener, &err);
	error_handler(err, __LINE__);
	printf("TID %x\n", collectorTemplateID);
	fBufSetInternalTemplate(collectorBuf, collectorTemplateID, &err);
	//simpleRecord_t simpleRecord; /* defined above */
	time_t lastalert = time(NULL);

	while (runthreads) {
		size_t length = sizeof(simpleRecord_t);

		if (nextrecord == NULL) {
			nextrecord = recpool[poolpos];
			//nextrecord = (simpleRecord_t*) malloc(length);
		}

		rc = fBufNext(collectorBuf, (uint8_t *)nextrecord, &length, &err);
		if (FALSE == rc) {
			cout << "NETFLOW ERROR:" << err->message << endl;
			if (!strncmp(err->message, "End of file", strlen("End of file"))) {
				g_clear_error(&err);
				fBufFree(collectorBuf);
				break;
			}
			g_clear_error(&err);

			continue;
		}
		if (flow_queue->size_approx() > FLOW_QUEUE_MAX) {
			if (time(NULL) - lastalert > 10) {
				lastalert = time(NULL);
				cout << "Flow queue grew too much " << flow_queue->size_approx() << " , dropping" << endl;
			}
			continue;
		} else {
			poolpos++;
			poolpos %= FLOW_QUEUE_MAX * 2;
		}

		flow_queue->enqueue(nextrecord);
		nextrecord = NULL;
	}
	for (int i = 0; i < FLOW_QUEUE_MAX * 2; i++) {
		free(recpool[i]);
	}

}

char *getCmdOption(char ** begin, char ** end, const std::string & option) {
	char ** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}
	return 0;
}

bool cmdOptionExists(char **begin, char **end, const std::string& option) {
	return std::find(begin, end, option) != end;
}

void MainClass::FlowProcessor(void) {
	simpleRecord_t *simpleRecord[BULK_RECORDS];
	char buffer_time[64];
	char buffer_filename[128];
	int seq = 0;
	time_t last_open;
	time_t current_time_offset;
	uint32_t current_record_offset;
	int fd = -1;
	uint32_t offset_map[TIME_QUANTUM];

	cout << "Staring FlowProcessor..." << endl;
	while (1) {
		if ((time(NULL) - last_open > TIME_QUANTUM || last_open > time(NULL)) && fd >= 0) {
			cout << "Closing old file..." << endl;
			// TODO: Write offset map
			lseek(fd, offsetof(f_header, offset_map), SEEK_SET);
			write(fd, &offset_map, sizeof(offset_map));
			close(fd);
			fd = -1;
		}
		if (fd < 0) {
			time_t t = time(NULL);
			struct tm *tmp = localtime(&t);
			if (tmp == NULL) {
				perror("localtime");
				exit(EXIT_FAILURE);
			}

			strftime (buffer_time, 64, "%Y%m%d-%H%M", tmp);
			sprintf(buffer_filename, "flows-%s-%d.bin", buffer_time, seq);
			seq++;
			last_open = time(NULL);
			current_time_offset = 0;
			current_record_offset = 0;
			cout << "Opening new file " << buffer_filename << endl;
			//fd = open(buffer_filename, O_RDWR | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR);
			fd = open(buffer_filename, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
			if (fd < 0) {
				cout << "Error opening records" << endl;
				exit(1);
			}
			// TODO write header
			struct f_header hdr;
			hdr.flags = f_mode;
			hdr.init_ts = last_open;
			// Test
			hdr.offset_map[0] = 0xde;
			hdr.offset_map[1] = 0xad;
			hdr.offset_map[2] = 0xbe;
			hdr.offset_map[3] = 0xef;
			write(fd, &hdr, sizeof(hdr));
			// TODO make it mmapped? we need to write map of timestamps

			// To avoid stalls
			continue;
		}
		size_t count = flow_queue->try_dequeue_bulk(simpleRecord, BULK_RECORDS);
		size_t ridx = 0;

		// flow_count
		flow_count += count;

		if (count == 0) {
			usleep(500);
			continue;
		}


		while (count > ridx) {
			bool save_record = true;
			// TODO: Maybe make lua engine to make conditions?
			if (matchip.size() > 0) {
				save_record = false;
				for (std::vector<string>::iterator it = matchip.begin(); it != matchip.end(); ++it) {
					if (IsIPInRangeExt(iptostring(simpleRecord[ridx]->sourceIPv4Address), *it) || IsIPInRangeExt(iptostring(simpleRecord[ridx]->destinationIPv4Address), *it)) {
						save_record = true;
						break;
					}
				}
			}
			if (matchsrc.size() > 0) {
				save_record = false;
				for (std::vector<string>::iterator it = matchsrc.begin(); it != matchsrc.end(); ++it) {
					if (IsIPInRangeExt(iptostring(simpleRecord[ridx]->sourceIPv4Address), *it)) {
						save_record = true;
						break;
					}
				}
			}
			if (matchdst.size() > 0) {
				save_record = false;
				for (std::vector<string>::iterator it = matchdst.begin(); it != matchdst.end(); ++it) {
					if (IsIPInRangeExt(iptostring(simpleRecord[ridx]->destinationIPv4Address), *it)) {
						save_record = true;
						break;
					}
				}
			}
			if (matchproto.size() > 0) {
				save_record = false;
				for (std::vector<int>::iterator it = matchproto.begin(); it != matchproto.end(); ++it) {
					if (simpleRecord[ridx]->protocolIdentifier == *it) {
						save_record = true;
						break;
					}
				}
			}
			if (matchport.size() > 0) {
				save_record = false;
				for (std::vector<int>::iterator it = matchport.begin(); it != matchport.end(); ++it) {
					if (simpleRecord[ridx]->sourceTransportPort == *it || simpleRecord[ridx]->destinationTransportPort == *it) {
						save_record = true;
						break;
					}
				}
			}
			if (matchdstport.size() > 0) {
				save_record = false;
				for (std::vector<int>::iterator it = matchdstport.begin(); it != matchdstport.end(); ++it) {
					if (simpleRecord[ridx]->destinationTransportPort == *it) {
						save_record = true;
						break;
					}
				}
			}
			if (matchsrcport.size() > 0) {
				save_record = false;
				for (std::vector<int>::iterator it = matchsrcport.begin(); it != matchsrcport.end(); ++it) {
					if (simpleRecord[ridx]->sourceTransportPort == *it) {
						save_record = true;
						break;
					}
				}
			}


			if (ignoreip.size() > 0) {
				save_record = false;
				for (std::vector<string>::iterator it = ignoreip.begin(); it != ignoreip.end(); ++it) {
					if (IsIPInRangeExt(iptostring(simpleRecord[ridx]->sourceIPv4Address), *it)) {
						save_record = false;
						break;
					}
					if (IsIPInRangeExt(iptostring(simpleRecord[ridx]->destinationIPv4Address), *it)) {
						save_record = false;
						break;
					}
				}
			}
			if (ignoresrc.size() > 0) {
				for (std::vector<string>::iterator it = ignoresrc.begin(); it != ignoresrc.end(); ++it) {
					if (IsIPInRangeExt(iptostring(simpleRecord[ridx]->sourceIPv4Address), *it)) {
						save_record = false;
						break;
					}
				}
			}
			if (ignoredst.size() > 0) {
				for (std::vector<string>::iterator it = ignoredst.begin(); it != ignoredst.end(); ++it) {
					if (IsIPInRangeExt(iptostring(simpleRecord[ridx]->destinationIPv4Address), *it)) {
						save_record = false;
						break;
					}
				}
			}
			if (ignoreproto.size() > 0) {
				for (std::vector<int>::iterator it = ignoreproto.begin(); it != ignoreproto.end(); ++it) {
					if (simpleRecord[ridx]->protocolIdentifier == *it) {
						save_record = false;
						break;
					}
				}
			}
			if (ignoreport.size() > 0) {
				for (std::vector<int>::iterator it = ignoreport.begin(); it != ignoreport.end(); ++it) {
					if (simpleRecord[ridx]->sourceTransportPort == *it || simpleRecord[ridx]->destinationTransportPort == *it) {
						save_record = false;
						break;
					}
				}
			}
			if (ignoredstport.size() > 0) {
				for (std::vector<int>::iterator it = ignoredstport.begin(); it != ignoredstport.end(); ++it) {
					if (simpleRecord[ridx]->destinationTransportPort == *it) {
						save_record = false;
						break;
					}
				}
			}
			if (ignoresrcport.size() > 0) {
				for (std::vector<int>::iterator it = ignoresrcport.begin(); it != ignoresrcport.end(); ++it) {
					if (simpleRecord[ridx]->sourceTransportPort == *it) {
						save_record = false;
						break;
					}
				}
			}

			if (save_record) {
				if (debug) {
					//cout << "NetFlow vs time(NULL) diff: " << time(NULL)-(simpleRecord[ridx]->observationTimeMilliseconds/1000) << " sec" << endl;
					cout << "Protocol " << unsigned(simpleRecord[ridx]->protocolIdentifier) << endl;
				}
				switch (f_mode) {
				// Full records
				case 1:
				{
					struct f_record r;
					// Check ports, NAPT ones too
					if (simpleRecord[ridx]->natEvent) {
						/*
							NAT Translation create - 1
							NAT Translation delete - 2
						*/
						r.type = simpleRecord[ridx]->natEvent;
					} else {
						r.type = simpleRecord[ridx]->flowEndReason;
					}
					r.portsrc = simpleRecord[ridx]->sourceTransportPort;
					r.natportsrc = simpleRecord[ridx]->postNAPTSourceTransportPort;
					r.portdst = simpleRecord[ridx]->destinationTransportPort;
					r.ipsrc = simpleRecord[ridx]->sourceIPv4Address;
					r.ipdst = simpleRecord[ridx]->destinationIPv4Address;
					r.ipnatsrc = simpleRecord[ridx]->postNATSourceIPv4Address;
					write(fd, &r, sizeof(r));
					// Debug
					//cout << "X " << int(simpleRecord[ridx]->octetDeltaCount) << endl;
				}
				break;
				// Brief records
				case 2:
				{
					struct f_record_brief r;
					r.type = simpleRecord[ridx]->natEvent;
					r.portsrc = simpleRecord[ridx]->sourceTransportPort;
					r.portdst = simpleRecord[ridx]->destinationTransportPort;
					r.ipsrc = simpleRecord[ridx]->sourceIPv4Address;
					r.ipdst = simpleRecord[ridx]->destinationIPv4Address;
					write(fd, &r, sizeof(r));
				}
				break;

				default:
					cout << "Invalid mode!" << endl;
					exit(2);
					break;
				}
			}
			simpleRecord[ridx] = NULL;
			current_record_offset++;
			if (time(NULL) - last_open != current_time_offset) {
				current_time_offset = time(NULL) - last_open;
				offset_map[current_time_offset] = current_record_offset;
			}
			ridx++;
		}
	}
}

// separate thread to serve as HTTP endpoint for metrics
void metrics_thread(MainClass *m) {
	int listen_sock, conn_sock;
	struct sockaddr_in servaddr;
	char buffer[1024];
	int n;
	time_t started;

	listen_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (listen_sock == -1) {
		perror("socket");
		exit(1);
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(m->metrics_port);

	if (bind(listen_sock, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
		perror("bind");
		exit(1);
	}

	if (listen(listen_sock, 10) == -1) {
		perror("listen");
		exit(1);
	}

	while (1) {
		int offset = 0;
		conn_sock = accept(listen_sock, NULL, NULL);
		if (conn_sock == -1) {
			perror("accept");
			exit(1);
		}

		/* primitive HTTP read, non-blocking read for 5 sec until \r\n\r\n */
		started = time(NULL);
		memset(buffer, 0, sizeof(buffer));
		while (1) {
			n = recv(conn_sock, buffer + offset, sizeof(buffer) - offset, MSG_DONTWAIT);
			if (n == -1) {
				if (errno == EAGAIN) {
					if (time(NULL) - started > 5) {
						break;
					}
					usleep(1000);
					continue;
				}
				perror("recv");
				exit(1);
			}
			if (n == 0) {
				break;
			}
			offset += n;
			if (offset > 4 && !strncmp(buffer + offset - 4, "\r\n\r\n", 4)) {
				break;
			}
			if (offset == sizeof(buffer)) {
				break;
			}
		}

		n = snprintf(buffer, sizeof(buffer), "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n");
		write(conn_sock, buffer, n);

		n = snprintf(buffer, sizeof(buffer), "flow_count {instance=\"%s\"} %lu\n", m->instance_name.c_str(), m->flow_count);
		write(conn_sock, buffer, n);

		close(conn_sock);
	}

}

int main(int argc, char *argv[]) {
	CLI::App app{"LE log storage"};
	std::string filename;
	MainClass *m = new (MainClass);

	app.add_flag("--netflow{true}", m->is_netflow, "Enable if you use Netflow v9, otherwise expect IPFIX");
	app.add_flag("--debug{true}", m->debug, "Emit debug messages");
	app.add_option("--ignoresrc", m->ignoresrc, "Ignore flow if SRC IP/netmask(s) match")->delimiter(',');
	app.add_option("--ignoredst", m->ignoredst, "Ignore flow if DST IP/netmask(s) match")->delimiter(',');
	app.add_option("--ignoreip", m->ignoreip, "Ignore flow if IP/netmask(s) present (numeric)")->delimiter(',');
	app.add_option("--ignoreproto", m->ignoreproto, "Ignore flow if protocol(s) present")->delimiter(',');
	app.add_option("--ignoreport", m->ignoreport, "Ignore flow if port(s) present")->delimiter(',');
	app.add_option("--ignoredstport", m->ignoredstport, "Ignore flow if dst port(s) present")->delimiter(',');
	app.add_option("--ignoresrcport", m->ignoresrcport, "Ignore flow if src port(s) present")->delimiter(',');
	app.add_option("--matchsrc", m->matchsrc, "Match flow if SRC IP/netmask(s) present")->delimiter(',');
	app.add_option("--matchdst", m->matchdst, "Match flow if DST IP/netmask(s) present")->delimiter(',');
	app.add_option("--matchip", m->matchip, "Match flow if IP/netmask(s) present")->delimiter(',');
	app.add_option("--matchproto", m->matchproto, "Match flow if protocol(s) present (numeric)")->delimiter(',');
	app.add_option("--matchport", m->matchport, "Match flow if port(s) present (numeric)")->delimiter(',');
	app.add_option("--matchsrcport", m->matchsrcport, "Match flow if src port(s) present (numeric)")->delimiter(',');
	app.add_option("--matchdstport", m->matchdstport, "Match flow if dst port(s) present (numeric)")->delimiter(',');
	app.add_option("--bindport", m->bindport, "Bind to port [2055]");
	app.add_option("--bindip", m->bindip, "Bind to IP [0.0.0.0]");
	app.add_option("--dir", m->dir, "Directory to save logging files");
	app.add_flag("--test", m->test, "Test mode, just run and exit 0");
	/* Metrics options */
	app.add_option("--metrics-port", m->metrics_port, "Metrics port [0 - off]");
	app.add_option("--instance-name", m->instance_name, "Instance name [leconnlog]");

	CLI11_PARSE(app, argc, argv);
	if (m->test) {
		cout << "Test mode, exiting" << endl;
		exit(0);
	}
	if (chdir(m->dir.c_str())) {
		cerr << "Cannot chdir to " << m->dir << endl;
		exit(1);
	}
	// TODO brief mode (less data)
	// TODO: validate data and optimize

	// TODO: remove this, test
	/*
	for(std::vector<string>::iterator it = m->ignoredst.begin(); it != m->ignoredst.end(); ++it) {
		cout << "Param: " << *it << endl;
	}
	exit(0);
	*/

	// Start metrics thread
	if (m->metrics_port > 0) {
		std::thread t1(metrics_thread, m);
		t1.detach();
	}

	std::thread t2([&]() {
		m->f_mode = 1;
		m->NetFlowThread();
	});
	t2.detach();
	//while (1) {
	m->FlowProcessor();
	//	sleep(1);
	//}
}