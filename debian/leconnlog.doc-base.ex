Document: leconnlog
Title: Debian leconnlog Manual
Author: <insert document author here>
Abstract: This manual describes what leconnlog is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/leconnlog/leconnlog.sgml.gz

Format: postscript
Files: /usr/share/doc/leconnlog/leconnlog.ps.gz

Format: text
Files: /usr/share/doc/leconnlog/leconnlog.text.gz

Format: HTML
Index: /usr/share/doc/leconnlog/html/index.html
Files: /usr/share/doc/leconnlog/html/*.html
