/*
Work in progress: web interface
*/
package main

import (
	"encoding/binary"
	"fmt"
	"io"
	"net"
	"os"
	"time"
)

const (
	// TIME_QUANTUM is the number of time quantums in the file
	TIME_QUANTUM = 600
)

// f_header is the header of the file
type f_header struct {
	sha512sum  [64]byte
	flags      uint32
	offset_map [TIME_QUANTUM]uint32
	init_ts    uint64
	entries    []f_record
}

// f_record is a record in the file
type f_record struct {
	type_      uint8
	portsrc    uint16
	portdst    uint16
	natportsrc uint16
	ipsrc      uint32
	ipdst      uint32
	ipnatsrc   uint32
}

func print_record(r *f_record) {
	var ipsrc net.IP
	var ipdst net.IP
	var ipnatsrc net.IP

	// TODO: Can we make this less uglier?
	ipsrc = net.IPv4(byte(r.ipsrc>>24), byte(r.ipsrc>>16), byte(r.ipsrc>>8), byte(r.ipsrc))
	ipdst = net.IPv4(byte(r.ipdst>>24), byte(r.ipdst>>16), byte(r.ipdst>>8), byte(r.ipdst))
	ipnatsrc = net.IPv4(byte(r.ipnatsrc>>24), byte(r.ipnatsrc>>16), byte(r.ipnatsrc>>8), byte(r.ipnatsrc))

	fmt.Printf("%d %s:%d(%s:%d) > %s:%d\n", r.type_, ipsrc, r.portsrc, ipnatsrc, r.natportsrc, ipdst, r.portdst)
}

func print_header(h *f_header) {
	fmt.Printf("sha512sum: %x\n", h.sha512sum)
	fmt.Printf("flags: %d\n", h.flags)
	fmt.Printf("offset_map: %d\n", h.offset_map)
	fmt.Printf("init_ts: %d\n", h.init_ts)
}

func read_header(r io.Reader) (*f_header, error) {
	var header f_header
	err := binary.Read(r, binary.LittleEndian, &header.sha512sum)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &header.flags)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &header.offset_map)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &header.init_ts)
	if err != nil {
		return nil, err
	}
	return &header, nil
}

func read_record(r io.Reader) (*f_record, error) {
	var record f_record
	err := binary.Read(r, binary.LittleEndian, &record.type_)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &record.portsrc)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &record.portdst)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &record.natportsrc)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &record.ipsrc)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &record.ipdst)
	if err != nil {
		return nil, err
	}
	err = binary.Read(r, binary.LittleEndian, &record.ipnatsrc)
	if err != nil {
		return nil, err
	}
	/*
		log.Printf("type: %d\n", record.type_)
		log.Printf("portsrc: %d\n", record.portsrc)
		log.Printf("portdst: %d\n", record.portdst)
		log.Printf("natportsrc: %d\n", record.natportsrc)
		log.Printf("ipsrc: %s\n", net.IP(record.ipsrc[:]))
		log.Printf("ipdst: %s\n", net.IP(record.ipdst[:]))
		log.Printf("ipnatsrc: %s\n", net.IP(record.ipnatsrc[:]))
	*/

	return &record, nil
}

func read_file(f *os.File) (*f_header, error) {
	var current_offset uint32
	var seq uint32
	header, err := read_header(f)
	if err != nil {
		return nil, err
	}
	print_header(header)
	current_offset = 0
	seq = 0
	// while not EOF
	for {
		//if header.offset_map[i] != 0 {
		//_, err = f.Seek(int64(i), 0)
		//if err != nil {
		//	return nil, err
		//}
		// retrieve current binary position
		if seq >= header.offset_map[current_offset] {
			current_offset = current_offset + 1
			fmt.Printf("ts: %d\n", current_offset)
			// print seq
			fmt.Printf("seq: %d\n", seq)
		}

		record, err := read_record(f)
		if err != nil {
			return nil, err
		}
		// print current offset as ts
		//fmt.Printf("ts: %d\n", current_offset)
		// print timestamp as [~Fri Jun  9 21:36:55 2023]
		// which is header.init_ts + current_offset
		t := time.Unix(int64(header.init_ts+uint64(current_offset)), 0)
		date_str := t.UTC().Format(time.RFC1123)

		fmt.Printf("[%s] ", date_str)
		print_record(record)
		header.entries = append(header.entries, *record)
		seq = seq + 1
		//}
		// shift by size of record
		//i = i + binary.Size(record)
	}
	//return header, nil
}

func open_file(path string) (*os.File, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return f, nil
}

func main() {
	f, err := open_file("test.bin")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	header, err := read_file(f)
	if err != nil {
		fmt.Println(err)
		return
	}
	print_header(header)
	for _, record := range header.entries {
		print_record(&record)
	}
}
