# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/leconnlog/issues
# Bug-Submit: https://github.com/<user>/leconnlog/issues/new
# Changelog: https://github.com/<user>/leconnlog/blob/master/CHANGES
# Documentation: https://github.com/<user>/leconnlog/wiki
# Repository-Browse: https://github.com/<user>/leconnlog
# Repository: https://github.com/<user>/leconnlog.git
