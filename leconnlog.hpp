#include <cstdint>
#include <string>
#define TIME_QUANTUM 600

uint32_t ip2uint(const std::string ip);
bool IsIPInRange(const std::string ip, const std::string network, const std::string mask);
bool IsIPInRangeExt(const std::string ip, std::string ipnetmask);
std::string iptostring (uint32_t ipAddress);

struct f_record {
    uint8_t type;
    uint16_t portsrc;
    uint16_t portdst;
    uint16_t natportsrc;
    uint32_t ipsrc;
    uint32_t ipdst;
    uint32_t ipnatsrc;
} __attribute__((packed));

struct f_record_brief {
    uint8_t type;
    uint16_t portsrc;
    uint16_t portdst;
    uint32_t ipsrc;
    uint32_t ipdst;
} __attribute__((packed));


struct f_header {
    uint8_t sha512sum[64];
    uint32_t flags;
    uint32_t offset_map[TIME_QUANTUM];
    uint64_t init_ts;
    struct f_record entries[];
} __attribute__((packed));

