# leconnlog
Highly efficient connections logging for legal purposes
Data storage format for connections is in leconnlog.hpp

## Building under Ubuntu
apt install build-essential libglib2.0-dev libfixbuf-dev cmake
mkdir build
cd build
cmake ..
make

Use binaries leconnview and leconnlog

## Building from source

* mkdir build
* cd build
* cmake ..
* make

Use binaries leconnview and leconnlog, for example:

```bash
cp leconnlog /usr/local/bin
cp leconnview /usr/local/bin
```

## IMPORTANT

You need to make sure correct timezone is set and NTP sync is properly working.

## Examples of usage

* leconnlog --ignoreproto=1 --matchsrc=172.16.0.0/12,123.111.111.0/24

This way daemon will ignore all connections with protocol 1 (ICMP) and will only log connections from
172.16.0.0/12 and 123.111.111.0/24 networks.

* leconnlog --ignoreproto=1 --matchsrc=172.16.0.0/12,123.111.111.0/24 --ignoredstport=123 --bindport=2055 --dir=/mnt/hdd/logs/other --netflow --ignoredst=185.89.87.0/24,185.22.34.0/24

This way daemon will ignore all connections with protocol 1 (ICMP) and will only log connections from
172.16.0.0/12,123.111.111.0/24 networks, will ignore all connections with destination port 123, and ignore data going to 185.89.87.0/24,185.22.34.0/24 (for example local subnets, CDN, etc)
It will listen on udp port 2055, expect netflow v9 packets and store them in /mnt/hdd/logs/other directory.

## Tested vendors

* Mikrotik
```
/ip traffic-flow
set enabled=yes interfaces=ether1 cache-entries=512k
/ip traffic-flow target
add dst-address=SERVERIP:2055 version=9
```
* ipt_NETFLOW (Linux)
```
modprobe ipt_NETFLOW natevents=1 destination="10.0.252.14:2055" protocol=9
# or
sysctl -w net.netflow.destination="10.0.252.14:2055"
sysctl -w net.netflow.protocol=9
```
* Cisco IOS
```
ip flow-export destination SERVERIP 2055
ip flow-export version 9

interface TenGigabitEthernet1/0/1
 ip flow ingress
 ip flow egress
```

## How to search for specific entry

Let's assume you need to find at Sep 15 01:52 2021 who connected to website with IP 202.168.105.30 over https.

You need to search filenames in log storage directory, pattern is:
```
flows-YYYYMMDD-hhmm-seq.bin
```
Where YYYYMMDD is year, month, day, hhmm is hour and minute when file started to log (new file usually created each 5-10 minutes), seq is sequence number of file.

So we need to search first record before Sep 15 01:52 2021, for example we have list of files:
```
-rw------- 1 root root 6628346 Sep 15  2021 flows-20210915-0105-36130.bin
-rw------- 1 root root 6132731 Sep 15  2021 flows-20210915-0115-36131.bin
-rw------- 1 root root 5374802 Sep 15  2021 flows-20210915-0125-36132.bin
-rw------- 1 root root 4944585 Sep 15  2021 flows-20210915-0135-36133.bin
-rw------- 1 root root 4840693 Sep 15  2021 flows-20210915-0145-36134.bin
-rw------- 1 root root 4941583 Sep 15  2021 flows-20210915-0155-36135.bin
```
It will be in file flows-20210915-0145-36134.bin in our case.

We run command to show all connections to this ip in this file:

```
leconnview -f flows-20210915-0145-36134.bin --ip 202.168.105.30

[~Wed Sep 15 01:52:02 2021] 1 172.17.82.60:42158(X.Y.Z.47:42158) > 202.168.105.30:443
[~Wed Sep 15 01:52:02 2021] 1 172.17.41.60:42504(X.Y.Z.51:42504) > 202.168.105.30:443
[~Wed Sep 15 01:52:02 2021] 2 172.17.93.79:34392(X.Y.Z.54:34392) > 202.168.105.30:443
[~Wed Sep 15 01:52:10 2021] 2 172.17.93.79:34381(X.Y.Z.54:34381) > 202.168.105.30:443
[~Wed Sep 15 01:52:10 2021] 2 172.17.37.199:29423(X.Y.Z.48:29423) > 202.168.105.30:443
[~Wed Sep 15 01:52:14 2021] 1 172.17.82.60:42210(X.Y.Z.47:42210) > 202.168.105.30:443
[~Wed Sep 15 01:52:14 2021] 1 172.17.93.79:34406(X.Y.Z.54:34406) > 202.168.105.30:443
[~Wed Sep 15 01:52:17 2021] 1 172.17.17.31:37060(X.Y.Z.60:37060) > 202.168.105.30:443
[~Wed Sep 15 01:52:17 2021] 1 172.17.17.31:37064(X.Y.Z.60:37064) > 202.168.105.30:443
[~Wed Sep 15 01:52:17 2021] 2 172.17.17.31:37060(X.Y.Z.60:37060) > 202.168.105.30:443
[~Wed Sep 15 01:52:17 2021] 1 172.17.37.199:29432(X.Y.Z.48:29432) > 202.168.105.30:443
[~Wed Sep 15 01:52:18 2021] 1 172.17.109.229:41492(X.Y.Z.40:41492) > 202.168.105.30:443
    
```
Where X.Y.Z.N is real NAT ip address. You can also skip --ip command and try to match records by grep tool.

## Docker

You can build docker image with docker/Dockerfile:

```
cd docker
docker build -t test-leconnlog .
sudo docker run -d -v /mnt/data:/storage test-leconnlog
```

Flow files will be stored in mapped volume /mnt/data

## TODO

* Make install, with automatic systemd service creation
* deb package for most common distros
* Add libhydrogen compiling to compile scripts properly, not just .a object.
* Add command line options for docker variant
* Add more examples
